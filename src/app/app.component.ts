import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // styleUrls: ['./app.component.css']
  styles: [`
    h3 {
      color: #b30912;
      // color: dodgerblue;
    }
  `]
})
export class AppComponent {
}
